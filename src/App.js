import React from "react";
import styled from "styled-components";

import Courses from "./components/Courses";
import Search from "./components/Search";

import { COURSES } from "./data";

const applyFilter = searchTerm => article =>
  article.title.toLowerCase().includes(searchTerm.toLowerCase());

const AppWrapper = styled.div`
  margin: 20px;
`;

const App = () => {
  const [searchTerm, setSearchTerm] = React.useState("");

  const handleSearch = event => {
    setSearchTerm(event.target.value);
  };

  return (
    <AppWrapper>
      <Search value={searchTerm} onSearch={handleSearch}>
        <p>Search</p>
      </Search>

      <Courses courses={COURSES.filter(applyFilter(searchTerm))} />
    </AppWrapper>
  );
};

export default App;
