import React from "react";
import styled from "styled-components";

const SearchWrapper = styled.div`
  display: flex;
  margin-bottom: 20px;
`;

const MyInput = styled.input`
  padding: 10px;
`;

const Search = ({ value, onSearch, children }) => (
  <SearchWrapper>
    {children} &nbsp;
    <MyInput value={value} onChange={onSearch} type="text" />
  </SearchWrapper>
);

export default Search;